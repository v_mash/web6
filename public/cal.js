function counter(listValue) 
{
    var counts = document.getElementsByClassName("counts");
    var count = document.getElementsByName("count");
    var equipment = document.getElementById("equipment");
    var options = document.getElementById("options");
    var selectedInputs;
    var markup = 0;
    var result1 = document.getElementById("result1");
    var r;
    switch (listValue) 
    {
        case "1":
            counts[0].style.display = "block";
            counts[1].style.display = "none";
            counts[2].style.display = "none";
            equipment.style.display = "none";
            options.style.display = "none";
            r = count[0].value * 7800;
            result1.innerHTML = (r.toString().match(/^[0-9]+$/) === null
                ? "Некорректный ввод"
                : "Итоговая стоимость: " + r + " руб.");
        break;
        case "2":
            counts[0].style.display = "none";
            counts[1].style.display = "block";
            counts[2].style.display = "none";
            equipment.style.display = "none";
            options.style.display = "block";
            selectedInputs = document.querySelectorAll("input[name=option]");
            selectedInputs.forEach(function (item) 
            {
                if (item.checked) 
                {
                    markup += parseInt(item.value);
                }
            });
            r = count[1].value * (6700 + markup);
            result1.innerHTML = (r.toString().match(/^[0-9]+$/) === null
                ? "Некорректный ввод"
                : "Итоговая стоимость: " + r + " руб.");
        break;
        case "3":
            counts[0].style.display = "none";
            counts[1].style.display = "none";
            counts[2].style.display = "block";
            equipment.style.display = "block";
            options.style.display = "none";
            selectedInputs = document.querySelectorAll("input[name=eq]");
            selectedInputs.forEach(function (item) 
            {
                if (item.checked) 
                {
                    markup += parseInt(item.value);
                }
            });
            r = count[2].value * (23200 + markup);
            result1.innerHTML = (r.toString().match(/^[0-9]+$/) === null
            ? "Некорректный ввод"
            : "Итоговая стоимость: " + r + " руб.");
        break;
    }
}

document.addEventListener("DOMContentLoaded", function () 
{
    var list = document.getElementsByName("list");
    counter(list[0].value);
    list[0].addEventListener("change", function () 
    {
        counter(list[0].value);
    });
    [].forEach.call(document.getElementsByTagName("input"), function (item) 
    {
        item.addEventListener("change", function () 
        {
            counter(list[0].value);
        });
    });
});
